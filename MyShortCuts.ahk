#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;=======;
; Utils ;
;=======;
::@@::
::lum@::
::lum::

;===============:
; Abbreviations ;
;===============;
:C:acc::acceptation
::av::avenue
:C:bat::bien à toi
:C:Bat::Bien à toi
:C:bav::bien à vous
:C:Bav::Bien à vous
::bkp::backup
:C:bjr::bonjour
:C:Bjr::Bonjour
::cad::c'est-à-dire?
::db::database
:C:dev::développement
::dict::dictionnaire
::env::environnement
::ex::exemple
:C:int::intégration
::lst::liste
::mtn::maintenant
::num::numéro
:C:prd::production
::,r::regards
::,R::Regards
:C:sim::simulation
::sry::sorry
::svp::s'il vous plaît
:C:thx::thanks
:C:Thx::Thanks
:C:thy::thank you
:C:Thy::Thank you
::tjs::toujours
:C:tst::test
::vol::volume

;===============;
; Programmation ;
;===============;
::,s::sudo

;===========;
; Fonctions ;
;===========;
#X::
    FormatTime, xx,, yyyMMdd
    SendInput %xx%
    return

;==============;
; Applications ;
;==============;
!c::
    run, "D:\Programmes\Microsoft VS Code\Code.exe"
    return
